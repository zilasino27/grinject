import csv


def straight_email(csv_file_name):
    filename = f'emails/{csv_file_name}.csv'
    pos = read_pos_file()  # read the current row position
    data = [row for row in csv.reader(open(filename, 'r'))]
    try:  # try to read email cell
        user_email = data[pos][1]
        write_pos_file(pos)
        print('picking from: ' + csv_file_name)
        print('picked email: ' + user_email)
        return user_email
    except Exception as e: # if email cell out of index reset current pos
        print(e)
        write_pos_file(current_pos=0) # reset current position
        # straight_swipe()
        print('all emails taken')


def read_pos_file():
    # read file to determine which email to use
    filename_pos = 'emails/openers_pos.csv'
    file = open(filename_pos, 'r')
    for row in file:
        position = int(row[0:-1]) # read whole cell and and convert to integer
        print('current csv position: ' + str(position))
    return position


def write_pos_file(current_pos):
    # read file to determine which email to use
    filename_pos = 'emails/openers_pos.csv'
    new_position = current_pos + 1
    row = [new_position]
    with open(filename_pos, 'w+') as f:
        writer = csv.writer(f)
        writer.writerow(row)
    return new_position


if __name__ == '__main__':
    straight_email('18k')