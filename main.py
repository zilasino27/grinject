from proxy_list_operations import pick_proxy_randomly
from proxy_browser import proxy_browser
from straight_email_picker import straight_email


def close_browser(driver):
    # Quit current browser and Start script again
    driver.close()
    driver.quit()


def got_to_browser():
    server = pick_proxy_randomly()                                  # Pick proxy randomly
    driver = proxy_browser(server)

    try:
        driver.set_page_load_timeout(30)
        driver.get("http://funnelymarketing.com/transaction-is-ready/")  # Open page
        picked_email = straight_email('list')                            # Get email address from function
        driver.find_element_by_xpath('//*[@id="ib2_el_hl9lldQ8"]/div/form/div[1]/div/input').send_keys(picked_email)
        driver.find_element_by_xpath('//*[@id="ib2_el_hl9lldQ8-submit"]').click()
        close_browser(driver)
    except Exception as e:
        print('erroras', e)
        close_browser(driver)
        got_to_browser()


if __name__ == '__main__':
    emails_to_add = 100
    while emails_to_add > 0:
        got_to_browser()
        emails_to_add -= 1