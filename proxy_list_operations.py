import json
import csv
import os
import random

file_path = os.path.dirname(os.path.abspath( __file__ ))


def pick_proxy_randomly():
    # pick proxy randomly from csv
    with open(file_path +'/proxies/all_proxies_zapier.csv') as f:
        reader = csv.reader(f)
        chosen_row = random.choice(list(reader))
        proxy = chosen_row[0]
        print('Randomly picked proxy: ' + proxy)
        return proxy


def save_to_csv(file_name, list):
    # take list and save to csv file all items in one column
    with open(f'{file_name}.csv', 'w') as f:  # Just use 'w' mode in 3.x
        w = csv.writer(f, lineterminator='\n')
        empty_list = ['empty'] # create it to add another row in csv file
        for item in list:
            w.writerow([item])


# open .json file with proxies
with open('server.json') as f:
    data = json.load(f)

proxies_us = []
proxies_uk = []
proxies_ca = []
proxies_all = []

# create lists with specific country proxies
for i in range(len(data)):
    if data[i]['country'] == 'United States':
        key = (data[i]['ip_address'])
        value = data[i]['country']
        proxies_us.append(key)
    elif data[i]['country'] == 'United Kingdom':
        key = (data[i]['ip_address'])
        value = data[i]['country']
        proxies_uk.append(key)
    elif data[i]['country'] == 'Canada':
        key = (data[i]['ip_address'])
        value = data[i]['country']
        proxies_ca.append(key)


# proxies_all = proxies_us + proxies_uk + proxies_ca

# save_to_csv('proxies/all_proxies_zapier', proxies_all)

if __name__ == '__main__':
    pick_proxy_randomly()


